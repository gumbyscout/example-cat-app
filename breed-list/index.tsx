import React, { useEffect, useState } from "react";
import { FlatList, Text } from "react-native";
import { List, Divider, ActivityIndicator } from "react-native-paper";

import { Breed, getBreeds } from "../breeds";

export default ({ navigation }) => {
  const [refreshing, setRefreshing] = useState(true);
  const [breeds, setBreeds] = useState<Breed[]>([]);
  useEffect(() => {
    getBreeds()
      .then((res) =>
        res.match({
          Ok: setBreeds,
          Err: console.error,
        })
      )
      .then(() => setRefreshing(false));
  }, []);
  return refreshing ? (
    <ActivityIndicator />
  ) : (
    <FlatList
      ItemSeparatorComponent={Divider}
      ListEmptyComponent={<Text>No cats found...</Text>}
      data={breeds}
      refreshing={refreshing}
      renderItem={({ item }) => (
        <List.Item
          title={item.name}
          onPress={() =>
            navigation.navigate("Breed Details", { name: item.name })
          }
        />
      )}
    />
  );
};
