import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import BreedList from "./breed-list";
import BreedDetails from "./breed-details";

const { Navigator, Screen } = createStackNavigator();

export default () => (
  <NavigationContainer>
    <Navigator initialRouteName="Breeds">
      <Screen name="Breeds" component={BreedList} />
      <Screen
        name="Breed Details"
        component={BreedDetails}
        options={({ route }) => ({ title: route.params.name })}
      />
    </Navigator>
  </NavigationContainer>
);
