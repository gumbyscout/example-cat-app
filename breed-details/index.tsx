import React, { useEffect, useState } from "react";
import { ScrollView } from "react-native";
import { Maybe } from "true-myth";
import { ActivityIndicator, Card, Title, Paragraph } from "react-native-paper";

import { Breed, getBreed, getImage } from "../breeds";

export default ({ route }) => {
  const { name } = route.params;
  const [image, setImage] = useState<Maybe<string>>(Maybe.nothing());
  const [breed, setBreed] = useState(Maybe.nothing<Breed>());

  useEffect(() => {
    getBreed(name).then((b) => {
      if (b.isErr()) {
        console.error(b.error);
        return;
      }
      setBreed(Maybe.just(b.value));

      getImage(b.value.id).then(setImage);
    });
  }, []);

  return (
    <ScrollView style={{ flex: 1 }}>
      {breed.match({
        Just: (breed) => (
          <Card>
            {image.isJust() ? (
              <Card.Cover source={{ uri: image.value }} />
            ) : null}
            <Card.Content>
              <Title>Description</Title>
              <Paragraph>{breed.description}</Paragraph>
            </Card.Content>
          </Card>
        ),
        Nothing: () => <ActivityIndicator />,
      })}
    </ScrollView>
  );
};
