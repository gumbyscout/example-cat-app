import { String, Record, Static, Runtype, Array } from "runtypes";
import { Result, Maybe } from "true-myth";

const API_KEY = "eb1afca3-e7fe-47a1-a20c-7d803e0e9ad0";

const Breed = Record({
  name: String,
  id: String,
  description: String,
});

export type Breed = Static<typeof Breed>;

export type Future<T, E> = Promise<Result<T, E>>;

const check =
  <T>(type: Runtype<T>) =>
  (object: unknown): Result<T, unknown> => {
    try {
      return Result.ok(type.check(object));
    } catch (e: unknown) {
      return Result.err(e);
    }
  };

export const getImage = (
  id: string,
  size: "full" | "thumb" | "small" | "med" = "full"
): Promise<Maybe<string>> =>
  fetch(
    "https://api.thecatapi.com/v1/images/search?" +
      new URLSearchParams({
        breed_id: id,
        size,
        limit: "1",
      })
  )
    .then((res) => res.json())
    .then(([img]) => img || {})
    .then((img) => Maybe.of(img.url));

export const getBreeds = (): Future<Breed[], unknown> =>
  fetch("https://api.thecatapi.com/v1/breeds", {
    headers: {
      "x-api-key": API_KEY,
    },
  })
    .then((res) => res.json())
    .then(check(Array(Breed)));

export const getBreed = (name: string): Future<Breed, unknown> =>
  fetch(
    "https://api.thecatapi.com/v1/breeds/search?" +
      new URLSearchParams({
        q: name,
      })
  )
    .then((res) => res.json())
    .then(([breed]) => breed)
    .then(check(Breed));
